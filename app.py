import json

from dataclasses import dataclass
from enum import Enum, auto
from http import HTTPStatus
from time import sleep
from typing import Optional, Union

import requests

from settings import API_KEY, BASE_URL, TARGET_URL, USER

HEADERS = {
    'X-Client-Api-key': API_KEY,
    'X-Client-User': USER,
}


class Method(str, Enum):
    GET = auto()
    HEAD = auto()
    POST = auto()
    PUT = auto()
    DELETE = auto()
    OPTIONS = auto()
    PATCH = auto()


METHODS_MAP = {
    Method.GET.name: requests.get,
    Method.HEAD.name: requests.head,
    Method.POST.name: requests.post,
    Method.PUT.name: requests.put,
    Method.DELETE.name: requests.delete,
    Method.OPTIONS.name: requests.options,
    Method.PATCH.name: requests.patch,
}


@dataclass
class Data:
    path: str
    method: Method
    headers: dict
    data: Optional[Union[str, dict]] = None


def _request():
    return requests.get(BASE_URL, headers=HEADERS)


def _request_target(data: Data):
    print('request', data)
    if data.method not in METHODS_MAP:
        print(f'Unknown method {data.method}')
        return
    try:
        if not data.data:
            body = None
        elif isinstance(data.data, str):
            body = data.data
        else:
            body = json.dumps(data.data)
        METHODS_MAP[data.method](
            TARGET_URL,
            data=body,
            headers=data.headers
        )
    except requests.HTTPError as err:
        print(f'Error: {err}')


def loop():
    while True:
        r = _request()
        if r.status_code == HTTPStatus.OK:
            if items := r.json():
                data = items.get('data')
                if isinstance(data, list):
                    for item in data:
                        print(item)
                        _request_target(Data(**item))
        else:
            print(f'Error: {r.status_code}')
        sleep(1)


def check_connection():
    response = _request()
    return response.status_code == HTTPStatus.OK


if __name__ == "__main__":
    if not check_connection():
        print('Connection failed')
        raise RuntimeError
    try:
        loop()
    except KeyboardInterrupt:
        print('exit')
