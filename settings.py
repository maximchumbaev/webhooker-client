from os import environ


BASE_URL = environ.get('BASE_URL', 'https://webhook.maxtech.space')
USER = environ.get('USER')
API_KEY = environ.get('API_KEY')
TARGET_URL = environ.get('TARGET_URL')
